import { Router } from "express";
import AuthController from "../controllers/auth.controller";
import { checkJwt } from "../middlewares/checkJwt";
const router =  Router();

router.post('/login',AuthController.login);
router.post('/register',AuthController.register);
router.post('/forgot',AuthController.forgot);
router.post('/newpassword',[checkJwt],AuthController.newPassword);

export default router;