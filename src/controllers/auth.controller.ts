import { PrismaClient } from "@prisma/client";
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { Response,Request } from "express";
import { validate } from "class-validator";
import Utils from "../config/utils";
import { transporter } from "../config/mailer";

//dktlvndmggkqznav

const prisma = new PrismaClient();


export default class AuthController {

    static login = async (req: Request, res: Response) => {

        try {
            //console.log(req.body)
            const { email, password } = req.body;

            const user = await prisma.user.findUnique({where:{ email : email }});
            // console.log(user);
            if (!user) return res.status(500)
            .send({ message: "Este correo electronico no esta registrado en nuestro sistema" });

            if (user.aproved == false) 
            return res.status(419).send({ message: "Su acceso aún no se ha gestionado"});

            const pass= Utils.checkIfUnencryptedPasswordIsValid(password,user.password);
            console.log(pass);
        
           if (!Utils.checkIfUnencryptedPasswordIsValid(password,user.password)) 
           return res.status(500).send({ message: "La contraseña es incorrecta" });


           const token = jwt.sign(
            {
                id: user.id,
                names:user.names,
                surnames:user.surnames,
                email:user.email
            },
            config.jwtSecret,
            { expiresIn: '1h' }
        );

        res.header('auth-token-cec', token).json({
            id: user.id,
            names:user.names,
            role: user.role,
            surnames:user.surnames,
            email:user.email,
            token:token}); 

            return res.json(user);

        } catch (error) {
            console.log(error)
        }
        

        
    }

    static register = async (req: Request, res: Response) => {

        try {
            console.log(req.body)
            const { names,role ,surnames, document_type,document,email,phone,password } = req.body;

            const verifyUsername = await prisma.user.findUnique({where:{ email :email }});
        if (verifyUsername) return res.status(500).send({ message: "El correo ya ha sido utilizado" });
        const errors = await validate(prisma.user);

        if (errors.length > 0) return res.status(400).send(errors);
    
        let hashPassword= Utils.hashPassword(password);;
    
        const user  = await prisma.user.create({
          data:{
            names : names,
            surnames:surnames,
            role : role,
            document_type:document_type,
            document:document,
            email:email,
            phone:phone,
            password:hashPassword
          }
        }) 
      
        console.log(user);
        return res.json(user);

        } catch (error) {
            console.log(error)
        }
        

        return res.json(req.body);
    }

    static forgot = async (req: Request, res: Response) => {
        try {

            //TODO: busca al usuario
            const { email } = req.body;

            const user = await prisma.user.findUnique({where:{ email : email }});
          

            if (!user) return res.status(500)
            .send({ message: "Este correo electronico no esta registrado en nuestro sistema" });


            const token = jwt.sign(
                {
                    id: user.id,
                    names:user.names,
                    surnames:user.surnames,
                    email:user.email
                },
                config.jwtSecret,
                { expiresIn: '1h' }
            );

            const bodyMail = `<b>Olvidaste tu contraseña? <a href=http://localhost:4200/auth/recovery/${token}>Click aquí</a> para asignar una nueva </b> `;
            
             await transporter.sendMail({
                from: '"Cambio de contraseña Coimnté ético citentífico 👻" <noreply@gmail.com>', // sender address
                to: "eduardo.a.arce@icloud.com", // list of receivers
                subject: "Cambiar contaseña cec 🔑", // Subject line
                text: "Hello world?", // plain text body
                html: bodyMail, // html body
              }).then(()=>{
                return res.status(200).json({message : "Se envió un correo de recuperación al e-mail indicado" });
              });
            
        } catch (error) {
            
        }
    }

    static newPassword = async (req: Request, res: Response) => {
        try {
            
            const id = res.locals.jwtPayload.id;
            const {  password } = req.body;

            let user = await prisma.user.findUnique({where:{id:id}});

            if (!user) return res.status(401).send();

            await prisma.user.update({
                where:{
                    id:id
                },
                data:{
                    password: Utils.hashPassword(password)
                }
            
            });

            return res.status(200).json({message : "Su contraseña se ha actualizado" });

            
            return res.json(id);
        } catch (error) {
            return res.json(error)
        }
        
    }



}